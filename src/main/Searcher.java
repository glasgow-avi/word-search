package main;

import dictionary.Data;
import dictionary.Initializer;

import java.util.LinkedList;

public class Searcher
{
	private static char[][] grid = new char[4][4];
	private static String arg;

	private static class Answer
	{
		private String word;
		private int score;

		static int[] scoreMap = {2, 5, 3, 3, 1, 5, 4, 4, 2, 10, 6, 3, 4, 2, 2, 4, 8, 2, 2, 2, 4, 6, 6, 9, 5, 8};

		public Answer(String word)
		{
			this.word = word;

			score = 0;
			for(int index = 0; index < word.length(); index++)
				score += scoreMap[word.charAt(index) - 97];
		}

		public static void sort(LinkedList<Answer> a)
		{
			boolean swap = true;
			while(swap)
			{
				swap = false;
				for(int i = 0; i < a.size() - 1; i++)
					for(int j = i + 1; j < a.size(); j++)
						if(a.get(j).score < a.get(i).score)
						{
							Answer x = a.get(j), y = a.get(i);
							a.remove(j);
							a.add(j, y);
							a.remove(i);
							a.add(i, x);
							swap = true;
						}
			}
		}
	}

	static
	{
		for(char a = 'a'; a <= 'z'; a++)
			Initializer.getLetter(a); //reads all the files
		System.out.println(Initializer.counter); //just counting how many words there are, nothing to see here
	}

	private static boolean lookAround(int seedRow, int seedCol, char nextChar) //looks if a character is present around a block
	{
		boolean res = false;
		try{res = res || nextChar == grid[seedRow - 1][seedCol - 1];} catch (ArrayIndexOutOfBoundsException e){}
		try{res = res || nextChar == grid[seedRow - 1][seedCol];} catch (ArrayIndexOutOfBoundsException e){}
		try{res = res || nextChar == grid[seedRow - 1][seedCol + 1];} catch (ArrayIndexOutOfBoundsException e){}
		try{res = res || nextChar == grid[seedRow][seedCol - 1];} catch (ArrayIndexOutOfBoundsException e){}
		try{res = res || nextChar == grid[seedRow][seedCol + 1];} catch (ArrayIndexOutOfBoundsException e){}
		try{res = res || nextChar == grid[seedRow + 1][seedCol - 1];} catch (ArrayIndexOutOfBoundsException e){}
		try{res = res || nextChar == grid[seedRow + 1][seedCol];} catch (ArrayIndexOutOfBoundsException e){}
		try{res = res || nextChar == grid[seedRow + 1][seedCol + 1];} catch (ArrayIndexOutOfBoundsException e){}

		return res;
	}

	private static boolean lookfor(char character, char nextCharacter) //do any occurances of a specified character have the next character as their neighbour
	{
		for(int rowIndex = 0; rowIndex < 4; rowIndex++)
			for(int colIndex = 0; colIndex < 4; colIndex++)
				if(grid[rowIndex][colIndex] == character && lookAround(rowIndex, colIndex, nextCharacter))
					return true;
		return false;
	}

	private static boolean find(String word)
	{
		for(int index = 0; index < word.length(); index++)
			if(!arg.contains(String.valueOf(word.charAt(index))))
				return false;

		for(int index = 0; index < word.length() - 1; index++)
			if(!lookfor(word.charAt(index), word.charAt(index + 1)))
				return false;

		return true;
	}

	public Searcher(String arg)
	{
		boolean[] lookFor = new boolean[26];

		this.arg = arg;

		for(char a = 'a'; a <= 'z'; a++)
			if(arg.contains(String.valueOf(a)))
				lookFor[a - 97] = true;

		for(int c = 0, rowIndex = 0; rowIndex < 4; rowIndex++)
			for(int colIndex = 0; colIndex < 4; colIndex++)
				grid[rowIndex][colIndex] = arg.charAt(c++);

		LinkedList<Answer> answers = new LinkedList<>();
		int words = 0;
		for(int i = 0; i < 26; i++)
		{
			if(!lookFor[i])
				continue;

			for(String word : Data.words[i])
				if(find(word))
				{
					answers.add(new Answer(word));
					words++;
				}
		}

		Answer.sort(answers);
		for(Answer answer : answers)
			System.out.println(answer.word + " " + answer.score);
		System.out.print(words);
	}

	public static void main(String[] args)
	{
		new Searcher(args[0]);
	}
}