package dictionary;

import java.io.BufferedReader;
import java.io.FileReader;

public class Initializer
{
	public static int counter = 0;

	public static void getLetter(char a)
	{
		String[] words = null;
		try (BufferedReader f = new BufferedReader(new FileReader("res/'" + String.valueOf(a) + "'")))
		{
			String file = "";
			for(String l = f.readLine(); l != null; l = f.readLine())
				file += l;

			words = file.split("\"");
		}
		catch (Exception r)
		{
			r.printStackTrace();
		}

		int i = 0;
		Data.words[a - 97] = new String[words.length / 2];
		for(String word : words)
			if(!(word.contains("[") || word.contains(",") || word.contains("]") ))
			{
				Data.words[a - 97][i++] = word;
				counter++;
			}
	}
}
