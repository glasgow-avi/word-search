package gui;

import main.Searcher;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Console extends JFrame
{
	public static String input = "";
	private static JTextField[][] fields = new JTextField[4][4];

	public Console()
	{
		setVisible(true);
		setLayout(new GridLayout(4, 4));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setFocusTraversalKeysEnabled(true);

		for(int rowIndex = 0; rowIndex < 4; rowIndex++)
			for(int colIndex = 0; colIndex < 4; colIndex++)
			{
				fields[rowIndex][colIndex] = new JTextField();
				add(fields[rowIndex][colIndex]);
				fields[rowIndex][colIndex].setPreferredSize(new Dimension(50, 50));
				fields[rowIndex][colIndex].addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e)
					{
						for(int rowIndex = 0; rowIndex < 4; rowIndex++)
							for(int colIndex = 0; colIndex < 4; colIndex++)
								input += fields[rowIndex][colIndex].getText();

						new Searcher(input);
					}
				});

				pack();
			}
	}

	public static void main(String[] args)
	{
		new Console();
	}
}